# Fast Tracker 

## Simple execise logging application
http://fasttrack1.blob.core.windows.net/workout/home.html 

Must use firefox for the calendar functionality at the moment. 

### End Points Used

**Get**  http://40.114.70.107/login  will check login credentials and return appropriate token if successful

**Post**  http://40.114.70.107/register will check if Username is in use and if free will register new user

**Get**  http://40.114.70.107/items  will return list of exercise log items

**Post**  http://40.114.70.107/items  will add new exercise log item

**Put**  http://40.114.70.107/items/id  will modify the record of exercise log item currently in the database

**Delete**  http://40.114.70.107/items/id will remove log item matching id from database




## General Usage and Goals
The main goal of this project was to develop an interactive calendar for logging and tracking exercise. 
