FROM java:8
COPY build/libs/workout-server-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar workout-server-1.0-SNAPSHOT.jar