package dev.pickett.controllers;

import dev.pickett.models.User;
import dev.pickett.services.UserService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class UserControllerTest {


    @InjectMocks
    private UserController userController;

    @Mock
    private UserService service = mock(UserService.class);

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void testGetAllUsersHandler(){

        Context context = mock(Context.class);

        List<User> list = new ArrayList<>();
        list.add(new User(1,"username","emai@no.com", "password1","austin","pickett",3));

        when(service.getAll()).thenReturn(list);//method stubbing
        userController.handleGetAllUsers(context);
        verify(context).json(list);


    }
    @Test
    void testSearchAllUsersHandler(){

        Context context = mock(Context.class);

        List<User> list = new ArrayList<>();
        list.add(new User(1,"username","emai@no.com", "password1","austin","pickett",3));

        when(context.formParam("query")).thenReturn("name");
        when(service.search("name")).thenReturn(list);//method stubbing
        userController.handleGetAllUsers(context);
        verify(context).json(list);


    }
    /*
    @Test
    void testGetUsersByIdHandler(){

        Context context = mock(Context.class);

       User d = new User(1,"username","emai@no.com", "password1","austin","pickett",3);
        when(context.pathParam("id")).thenReturn("1");
        when(service.getById(1)).thenReturn(d);//method stubbing
        userController.handleGetUserById(context);
        verify(context).json(d);


    }
    */
}
