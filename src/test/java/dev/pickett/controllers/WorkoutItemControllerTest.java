package dev.pickett.controllers;

import dev.pickett.models.User;
import dev.pickett.models.WorkoutItem;
import dev.pickett.services.WorkoutItemService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class WorkoutItemControllerTest {
/*
    @InjectMocks
    private WorkoutItemController itemController;

    @Mock
    private WorkoutItemService service = mock(WorkoutItemService.class);

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void testGetAllItemsHandler(){

        Context context = mock(Context.class);

        List<WorkoutItem> list = new ArrayList<>();
        list.add(new WorkoutItem(1,1.2,"burpee",3,3,new User(5),null));
        list.add(new WorkoutItem(2,2.2,"Sit Ups",3,3,new User(5),null));
        list.add(new WorkoutItem(3,3.2,"Push Ups",3,3,new User(3),null));

        //when(context.queryParam("query")).thenReturn("name");
        when(service.getAll()).thenReturn(list);//method stubbing
        itemController.handleGetAllItems(context);
        verify(context).json(list);


    }

    @Test
    void testGetSearchItemsHandler(){

        Context context = mock(Context.class);

        List<WorkoutItem> list = new ArrayList<>();
        list.add(new WorkoutItem(1,1.2,"burpee",3,3,new User(5),null));
        list.add(new WorkoutItem(2,2.2,"Sit Ups",3,3,new User(5),null));
        list.add(new WorkoutItem(3,3.2,"Push Ups",3,3,new User(3),null));

        when(context.queryParam("query")).thenReturn("burpee");
        when(service.getAll()).thenReturn(list);//method stubbing
        itemController.handleGetAllItems(context);
        verify(context).json(list);


    }
    @Test
    void testGetItemByIdHandler(){

        Context context = mock(Context.class);
        WorkoutItem d = new WorkoutItem(1,1.2,"burpee",3,3,new User(5),null);


        when(context.pathParam("id")).thenReturn("1");
        when(service.getById(1)).thenReturn(d);//method stubbing
        itemController.handleGetItemById(context);
        verify(context).json(d);
    }

*/


}
