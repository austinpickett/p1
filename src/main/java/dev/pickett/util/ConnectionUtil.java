package dev.pickett.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {


    private static Connection connection;
    private static final String CONNECTION_USERNAME = System.getenv("username");
    private static final String CONNECTION_PASSWORD = System.getenv("password");
    private static final String connectionURL = "jdbc:postgresql://localhost:5432/postgres";


public static Connection getConnection() throws SQLException {

    if(connection ==null){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        connection = DriverManager.getConnection(connectionURL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
    }else if (connection.isClosed()) {

        connection = DriverManager.getConnection(connectionURL, CONNECTION_USERNAME, CONNECTION_PASSWORD);


    }
        return connection;

    }
}
