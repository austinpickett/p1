package dev.pickett.services;

import dev.pickett.data.WorkoutItemDAO;
import dev.pickett.data.WorkoutItemDAOHibImpl;
import dev.pickett.models.User;
import dev.pickett.models.WorkoutItem;

import java.time.LocalDate;
import java.util.List;

public class WorkoutItemService {

    private final WorkoutItemDAO dao = new WorkoutItemDAOHibImpl();

    public List<WorkoutItem> getByDate(int userID, LocalDate date){return dao.getWorkoutItemsByDate(userID,date);}
    public List<WorkoutItem> getAll(){return dao.getAllWorkoutItems();}
    public WorkoutItem getById(int id){return dao.getWorkoutItemById(id);}

    public List<WorkoutItem> getByName(String name){return dao.getWorkoutItemsByName(name);}
    public List<WorkoutItem> getByName(String name, User user){return dao.getWorkoutItemsByName(name,user.getId());}

    public WorkoutItem add(WorkoutItem item){return dao.addNewWorkoutItem(item);}

    public WorkoutItem update(WorkoutItem item){return dao.updateWorkoutItem(item);}

    public void delete(int id){dao.deleteWorkoutItem(id);}


}
