package dev.pickett.controllers;


import dev.pickett.models.User;
import dev.pickett.models.WorkoutItem;
import dev.pickett.services.WorkoutItemService;
import dev.pickett.util.AuthTokenUtil;
import io.javalin.http.BadGatewayResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import kong.unirest.json.JSONException;
import kong.unirest.json.JSONObject;
import org.eclipse.jetty.util.ajax.JSON;
import org.eclipse.jetty.util.ajax.JSONObjectConvertor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.InputStream;
import java.time.LocalDate;
import java.util.Date;

public class WorkoutItemController {

        Logger logger= LoggerFactory.getLogger(WorkoutItemController.class);
        private WorkoutItemService service = new WorkoutItemService();

        public void handleGetAllItems(Context ctx){
                LocalDate date;

                if(ctx.header("q-date")!=null){
                        date = LocalDate.parse(ctx.header("q-date"));
                }else{
                        date = LocalDate.now();
                }
                ctx.json(service.getByDate(AuthTokenUtil.verifyAuthToken(ctx.header("authorization")),date));

        }

        public void handleGetItemById(Context ctx){

                int id = Integer.parseInt(ctx.pathParam("id"));
                WorkoutItem d = service.getById(id);
                if(d==null){
                        logger.warn("no item present with id: {}",id);
                        throw new NotFoundResponse();
                }else{
                        ctx.json(d);
                }
        }
        public void handlePostItem(Context ctx){
                int sets=0,reps =0,userID =0;
                double calories =0.0;

                JSONObject json = new JSONObject(ctx.body());
                try{
                        String name = json.getString("name");
                        sets = json.getInt("sets");
                        reps = json.getInt("reps");
                        calories = json.getDouble("calories");
                        userID = AuthTokenUtil.verifyAuthToken(ctx.header("Authorization"));
                        LocalDate date = LocalDate.parse(json.getString("date"));

                        service.add(new WorkoutItem(0,calories,name,sets,reps,new User(userID),date));//201 created
                        ctx.status(201);

                }catch(JSONException e){
                        logger.error("issues parsing post item body");
                }




        }
        public void handlePutItem(Context ctx){
                int id=0,sets=0,reps =0,userID =0;
                double calories =0.0;

                JSONObject json = new JSONObject(ctx.body());
                try{
                        id = json.getInt("id");
                        String name = json.getString("name");
                        sets = json.getInt("sets");
                        reps = json.getInt("reps");
                        calories = json.getDouble("calories");
                        userID = AuthTokenUtil.verifyAuthToken(ctx.header("Authorization"));
                        LocalDate date = LocalDate.parse(json.getString("date"));
                        service.update(new WorkoutItem(id,calories,name,sets,reps,new User(userID),date));//201 created
                        ctx.status(201);

                }catch(JSONException e){
                        logger.error("issues parsing post item body");
                }
                /*

                //WILL NEED TO ENFORCE NONE NULL numbers (default submit of 0 or 0.0)
                int id = Integer.parseInt(ctx.formParam("id"));
                double calories = parseWithDefault(ctx.formParam("calories"),0.0);
                String name = ctx.formParam("name");
                int sets = parseWithDefault(ctx.formParam("sets"),0);
                int reps = parseWithDefault(ctx.formParam("reps"),0);
                int userID = parseWithDefault(ctx.formParam("userID"),0);
                LocalDate date = LocalDate.parse(ctx.formParam("date"));
                WorkoutItem updatedItem = service.update(new WorkoutItem(id,calories,name,sets,reps,new User(userID),date));
                ctx.status(201);
                ctx.json(updatedItem);

                 */

        }
        public void handleDeleteItem(Context ctx){
                System.out.println("delete path");
                String idString = ctx.pathParam("id");
                if(idString.matches("^\\d+$")){
                        int id = Integer.parseInt(idString);
                        service.delete(id);
                }else{
                        throw new BadGatewayResponse("Input \""+idString+"\" cannot be parsed to an int");
                }

        }
        public static int parseWithDefault(String s, int defaultVal) {
                if(s==null){
                        return 0;
                }
                return s.matches("-?\\d+") ? Integer.parseInt(s) : defaultVal;
        }
        public static Double parseWithDefault(String s, double defaultVal) {
                if(s==null){
                        return 0.0;
                }
                return s.matches("-?\\d+") ? Double.parseDouble(s) : defaultVal;
        }



}
