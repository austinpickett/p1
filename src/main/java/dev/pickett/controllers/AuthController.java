package dev.pickett.controllers;

import dev.pickett.models.User;
import dev.pickett.models.WorkoutItem;
import dev.pickett.util.AuthTokenUtil;
import dev.pickett.util.ConnectionUtil;
import dev.pickett.util.HibernateUtil;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthController {

    Logger logger = LoggerFactory.getLogger(AuthController.class);
    private Connection connection = null;
    private PreparedStatement stmt = null;

    public void authenticateLogin(Context ctx){
        // getting params from what would be a form submission (content-type: application/x-www-form-urlencoded)

        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        System.out.println(user+"  "+pass);
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);

            Root<User> root = cq.from(User.class);
            cq.select(root).where(
                    cb.equal(root.get("username"),user),
                    cb.equal(root.get("password"),pass)
            );
            Query<User> query = s.createQuery(cq);
            User wi = query.getSingleResult();
            if(wi!=null){
                ctx.header("Authorization", AuthTokenUtil.getAuthToken(String.valueOf(wi.getId())));
                ctx.status(200);
                logger.info("{} successful login", user);
                return;
            }

        }
/*
        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("SELECT * FROM users WHERE username=? AND pass=?");
            stmt.setString(1,user);
            stmt.setString(2,pass);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                int id = rs.getInt(rs.getInt("id"));
                logger.error("auth id {}",id);

               // ctx.header("Access-Control-Expose-Headers", "Authorization");
                ctx.header("Authorization", AuthTokenUtil.getAuthToken(String.valueOf(id)));
              //  ctx.header("UserID",String.valueOf(rs.getInt("id")));
                ctx.status(200);
                logger.info("{} successful login", user);
                return;
            }


        }catch(SQLException e){
            logger.error("SQl exception when attempting to log in for {}",user);
        }finally{
            closeResources();
        }
        logger.warn("{} failed login",user);
        throw new UnauthorizedResponse("Credentials were incorrect");


 */
    }
    public void authorizeToken(Context ctx){

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String authHeader =ctx.header("Authorization");
        if(authHeader!=null && AuthTokenUtil.verifyAuthToken(authHeader)!=-1){
            logger.info("request is authorized, proceeding to handler method");
        }else{
            logger.warn("improper authorization");
            throw new UnauthorizedResponse();
        }
    }
    private void logExceptions(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }

    private void closeResources() {
        try{
            if(stmt !=null){
                stmt.close();
            }
        } catch (SQLException e) {
            logExceptions(e);
        }
        try{
            if(connection != null){
                connection.close();
            }
        }catch(SQLException e){
            logExceptions(e);
        }
    }
}
