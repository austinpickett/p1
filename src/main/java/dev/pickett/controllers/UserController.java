package dev.pickett.controllers;


import dev.pickett.models.User;
import dev.pickett.services.UserService;
import dev.pickett.util.AuthTokenUtil;
import io.javalin.http.BadGatewayResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserController {

    Logger logger= LoggerFactory.getLogger(UserController.class);
    private UserService service = new UserService();

    public void handleGetAllUsers(Context ctx){
        if(ctx.formParam("query")==null) {
            ctx.json(service.getAll());
        }else{
            ctx.json(service.search(ctx.formParam("query")));
        }
    }
    public void handleGetUserById(Context ctx){

        User d = service.getById(Integer.parseInt(ctx.pathParam("id")));
        if(d==null){
            logger.warn("no item present with requested id");
            throw new NotFoundResponse();
        }else{
            ctx.json(d);
        }
    }
    public void handlePostUser(Context ctx){

        //int id = Integer.parseInt(ctx.formParam("id"));
        String username = ctx.formParam("username");
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");
        String firstname = ctx.formParam("firstName");
        String lastname = ctx.formParam("lastName");
        int auth = Integer.parseInt(ctx.formParam("auth"));
        User d = new User(0, username, email, password,firstname, lastname, auth);
        User n = service.add(d);//201 created
        System.out.println(n.getId()+"    this is my id reg");
        ctx.header("Authorization", AuthTokenUtil.getAuthToken(String.valueOf(n.getId())));

        ctx.status(201);
    }
    public void handlePutUser(Context ctx){
        int id = Integer.parseInt(ctx.formParam("id"));
        String username = ctx.formParam("username");
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");
        String firstname = ctx.formParam("firstName");
        String lastname = ctx.formParam("lastName");
        int auth = Integer.parseInt(ctx.formParam("auth"));
        User d = new User(id, username, email, password,firstname, lastname, auth);
        User updatedUser = service.update(d);
        ctx.status(201);
        ctx.json(updatedUser);

    }
    public void handleDeleteUser(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int id = Integer.parseInt(idString);
            service.delete(id);
        }else{
            throw new BadGatewayResponse("Input \""+idString+"\" cannot be parsed to an int");
        }

    }

}
