package dev.pickett.data;

import dev.pickett.models.WorkoutItem;

import java.time.LocalDate;
import java.util.List;

public interface WorkoutItemDAO {

    List<WorkoutItem> getAllWorkoutItems();
    List<WorkoutItem> getWorkoutItemsByDate(int userID, LocalDate date);
    WorkoutItem getWorkoutItemById(int id);
    List<WorkoutItem> getWorkoutItemsByName(String name);
    List<WorkoutItem> getWorkoutItemsByName(String name,int userID);
    WorkoutItem updateWorkoutItem(WorkoutItem item);
    WorkoutItem addNewWorkoutItem(WorkoutItem item);
    void deleteWorkoutItem(int id);

}
