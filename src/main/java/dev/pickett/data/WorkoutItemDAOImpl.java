package dev.pickett.data;

import dev.pickett.models.User;
import dev.pickett.models.WorkoutItem;
import dev.pickett.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class WorkoutItemDAOImpl implements WorkoutItemDAO {

    private final Logger logger = LoggerFactory.getLogger(WorkoutItemDAOImpl.class);
    private Connection connection = null;
    private PreparedStatement stmt = null;




    public List<WorkoutItem> getAllWorkoutItems(){

        List<WorkoutItem> list = new ArrayList<>();

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("select * from workoutitem");
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                int id = rs.getInt(WorkoutItemMapping.ID);
                double calories = rs.getDouble(WorkoutItemMapping.CAL);
                String name = rs.getString(WorkoutItemMapping.NAME);
                int sets = rs.getInt(WorkoutItemMapping.SETS);
                int reps = rs.getInt(WorkoutItemMapping.REPS);
                int userID = rs.getInt(WorkoutItemMapping.USERID);
                LocalDate date = rs.getObject(7,LocalDate.class);
                list.add(new WorkoutItem(id,calories,name,sets,reps,new User(userID),date));
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        logger.info("retrieved {} items from the database",list.size());
        return list;

    }

    @Override
    public List<WorkoutItem> getWorkoutItemsByDate(int userID, LocalDate date) {
        return null;
    }

    public List<WorkoutItem> getAllWorkoutItems(int userID){

        List<WorkoutItem> list = new ArrayList<>();

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("select * from workout_item WHERE userid=?");
            stmt.setInt(1,userID);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                int id = rs.getInt(WorkoutItemMapping.ID);
                double calories = rs.getDouble(WorkoutItemMapping.CAL);
                String name = rs.getString(WorkoutItemMapping.NAME);
                int sets = rs.getInt(WorkoutItemMapping.SETS);
                int reps = rs.getInt(WorkoutItemMapping.REPS);

                LocalDate date = rs.getObject(7,LocalDate.class);
                list.add(new WorkoutItem(id,calories,name,sets,reps,new User(userID),date));
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        logger.info("retrieved {} items from the database",list.size());
        return list;

    }

    public WorkoutItem getWorkoutItemById(int id){
        //example of stream
        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("select * from workout_item where id = ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                double calories = rs.getDouble(WorkoutItemMapping.CAL);
                String name = rs.getString(WorkoutItemMapping.NAME);
                int sets = rs.getInt(WorkoutItemMapping.SETS);
                int reps = rs.getInt(WorkoutItemMapping.REPS);
                int userID = rs.getInt(WorkoutItemMapping.USERID);
                LocalDate date = rs.getObject(7,LocalDate.class);
                logger.info("1 item retrieved from database by id: {}",id);
                return new WorkoutItem(id,calories,name,sets,reps,new User(userID),date);
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
         return null;
    }

    @Override
    public List<WorkoutItem> getWorkoutItemsByName(String name) {

        List<WorkoutItem> list = new ArrayList<>();

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("SELECT * FROM workout_item WHERE name LIKE '%'||?||'%'");
            stmt.setString(1,name);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                int id = rs.getInt(WorkoutItemMapping.ID);
                double calories = rs.getDouble(WorkoutItemMapping.CAL);
                String returnedName = rs.getString(WorkoutItemMapping.NAME);
                int sets = rs.getInt(WorkoutItemMapping.SETS);
                int reps = rs.getInt(WorkoutItemMapping.REPS);
                int userID = rs.getInt(WorkoutItemMapping.USERID);
                LocalDate date = rs.getObject(7,LocalDate.class);
                list.add(new WorkoutItem(id,calories,returnedName,sets,reps,new User(userID),date));

            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        logger.info("retrieved {} matches for {} in database",list.size(),name);
        return list;
    }

    public List<WorkoutItem> getWorkoutItemsByName(String name,int userID) {

        List<WorkoutItem> list = new ArrayList<>();

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("SELECT * FROM workout_item WHERE name LIKE '%'||?||'%' AND userid=?");
            stmt.setString(1,name);
            stmt.setInt(2,userID);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                int id = rs.getInt(WorkoutItemMapping.ID);
                double calories = rs.getDouble(WorkoutItemMapping.CAL);
                String returnedName = rs.getString(WorkoutItemMapping.NAME);
                int sets = rs.getInt(WorkoutItemMapping.SETS);
                int reps = rs.getInt(WorkoutItemMapping.REPS);

                LocalDate date = rs.getObject(7,LocalDate.class);
                list.add(new WorkoutItem(id,calories,returnedName,sets,reps,new User(userID),date));

            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        logger.info("retrieved {} matches for {} in database",list.size(),name);
        return list;
    }

    public WorkoutItem addNewWorkoutItem(WorkoutItem item){

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("insert into workout_item (calories,name,sets,reps,userid,date) values(?,?,?,?,?,?)");

            stmt.setDouble(1,item.getCalories());
            stmt.setString(2,item.getName());
            stmt.setInt(3,item.getSets());
            stmt.setInt(4,item.getReps());
            stmt.setObject(5,item.getUser());
            stmt.setObject(6, LocalDate.now());


            if(stmt.executeUpdate() != 0){
                logger.info("successfully added new item");
                return item;
            }else{
                logger.error("there was an issue adding the item to the database");
                return null;
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        return null;
    }
    public WorkoutItem updateWorkoutItem(WorkoutItem item){

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("UPDATE workout_item SET calories=?, name=?, sets=?, reps=?, userid=?, date=? WHERE id=?");

            stmt.setDouble(1,item.getCalories());
            stmt.setString(2,item.getName());
            stmt.setInt(3,item.getSets());
            stmt.setInt(4,item.getReps());
            stmt.setObject(5,item.getUser());
            stmt.setObject(6, item.getDate());
            stmt.setInt(7,item.getId());

            if(stmt.executeUpdate() != 0){
                logger.info("successfully updated item");
                return item;
            }else{
                logger.error("there was an issue updating the item to the database");
                return null;
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        return null;
    }
    public void deleteWorkoutItem(int id){
        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("DELETE FROM workout_item WHERE id=?");
            stmt.setInt(1,id);
            if(stmt.executeUpdate() != 0){
                logger.info("successfully removed item");

            }else{
                logger.error("there was an issue removing the item from the database");

            }
        }catch(SQLException e){
            logExceptions(e);

        }finally{
            closeResources();
        }
    }
    private void closeResources() {
        try{
            if(stmt !=null){
                stmt.close();
            }
        } catch (SQLException e) {
            logExceptions(e);
        }
        try{
            if(connection != null){
                connection.close();
            }
        }catch(SQLException e){
            logExceptions(e);
        }
    }
    private void logExceptions(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }
    private static class WorkoutItemMapping {
        private static final String ID = "id";
        private static final String CAL = "calories";
        private static final String NAME = "name";
        private static final String SETS = "sets";
        private static final String REPS = "reps";
        private static final String USERID = "userID";

    }


}
