package dev.pickett.data;

import dev.pickett.models.User;
import dev.pickett.models.WorkoutItem;
import dev.pickett.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

public class WorkoutItemDAOHibImpl implements WorkoutItemDAO{

    private final Logger logger = LoggerFactory.getLogger(WorkoutItemDAOHibImpl.class);

    @Override
    public List<WorkoutItem> getAllWorkoutItems() {

        try(Session s = HibernateUtil.getSession()){
            List<WorkoutItem> items = s.createQuery("from WorkoutItem",WorkoutItem.class).list();
            return items;
        }
    }

    @Override
    public List<WorkoutItem> getWorkoutItemsByDate(int userID,LocalDate date) {
        System.out.println("get items  by date with id "+userID+" date:"+date.toString());
        //grabs only the supplied date
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<WorkoutItem> cq = cb.createQuery(WorkoutItem.class);

            Root<WorkoutItem> root = cq.from(WorkoutItem.class);
            cq.select(root);

            cq.where(
                    cb.equal(root.get("date"),date),
                    cb.equal(root.get("user"),new User(userID)));

            Query<WorkoutItem> query = s.createQuery(cq);
            return query.list();

        }

        /*
        //This one grabs a month range
        LocalDate d1 = date.withDayOfMonth(1);
        LocalDate d2 = d1.plusMonths(1);

        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<WorkoutItem> cq = cb.createQuery(WorkoutItem.class);

            Root<WorkoutItem> root = cq.from(WorkoutItem.class);
            cq.select(root);

            cq.where(cb.between(root.get("date"),d1, d2));

            Query<WorkoutItem> query = s.createQuery(cq);
            return query.list();

        }
        */
    }

    @Override
    public WorkoutItem getWorkoutItemById(int id) {
        try(Session s = HibernateUtil.getSession()){
            WorkoutItem item = s.get(WorkoutItem.class,id);
            return item;
        }

    }

    @Override
    public List<WorkoutItem> getWorkoutItemsByName(String name) {

        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<WorkoutItem> cq = cb.createQuery(WorkoutItem.class);

            Root<WorkoutItem> root = cq.from(WorkoutItem.class);
            cq.select(root);

            cq.where(cb.like(root.get("name"),name));
            Query<WorkoutItem> query = s.createQuery(cq);
            return query.list();

        }
    }

    @Override
    public List<WorkoutItem> getWorkoutItemsByName(String name, int userID) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<WorkoutItem> cq = cb.createQuery(WorkoutItem.class);

            Root<WorkoutItem> root = cq.from(WorkoutItem.class);
            cq.select(root).where(
                    cb.equal(root.get("id"),userID),
                    cb.like(root.get("name"),name)
            );
            Query<WorkoutItem> query = s.createQuery(cq);
            return query.list();

        }
    }

    @Override
    public WorkoutItem updateWorkoutItem(WorkoutItem item) {
        System.out.println("updateWorkoutitem");
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.update(item);
            logger.info("updated item with id: {}",item.getId());
            tx.commit();
            return item;
        }
    }

    @Override
    public WorkoutItem addNewWorkoutItem(WorkoutItem item) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(item);
            logger.info("added new item with id: {}",id);
            tx.commit();
            item.setId(id);
            return item;
        }
    }

    @Override
    public void deleteWorkoutItem(int id) {
        System.out.println("deleting item "+id);
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            WorkoutItem d = new WorkoutItem(id);
            d.setId(id);
            s.delete(d);
            logger.info("deleted item with id: {}",id);
            tx.commit();
        }
    }
}
