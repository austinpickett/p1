package dev.pickett.data;

import dev.pickett.models.User;
import dev.pickett.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO{
    private final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);
    private Connection connection = null;
    private PreparedStatement stmt = null;

    @Override
    public List<User> getAllUsers() {
        List<User> list = new ArrayList<>();

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("select * from users");
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                int id = rs.getInt(UserDAOImpl.UserMapping.ID);
                String username = rs.getString(UserDAOImpl.UserMapping.USERNAME);
                String email = rs.getString(UserDAOImpl.UserMapping.EMAIL);
                String pass = "EMPTY RESULT";
                String fName = rs.getString(UserDAOImpl.UserMapping.FNAME);
                String lName = rs.getString(UserDAOImpl.UserMapping.LNAME);
                int auth = rs.getInt(UserDAOImpl.UserMapping.AUTH);
                list.add(new User(id,username,email,pass,fName,lName,auth));
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        logger.info("retrieved {} Users from the database",list.size());
        return list;
    }

    @Override
    public User getUserById(int id) {
        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("select * from users where id = ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                String username = rs.getString(UserDAOImpl.UserMapping.USERNAME);
                String email = rs.getString(UserDAOImpl.UserMapping.EMAIL);
                String pass = "EMPTY RESULT";
                String fName = rs.getString(UserDAOImpl.UserMapping.FNAME);
                String lName = rs.getString(UserDAOImpl.UserMapping.LNAME);
                int auth = rs.getInt(UserDAOImpl.UserMapping.AUTH);
                logger.info("1 item retrieved from database by id: {}",id);
                return new User(id,username,email,pass,fName,lName,auth);
            }

        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        return null;
    }

    @Override
    public List<User> searchUsers(String query) {
        List<User> list = new ArrayList<>();

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("SELECT * FROM users WHERE username LIKE '%'||?||'%'" +
                    "OR firstname LIKE '%'||?||'%' " +
                    "OR lastname LIKE '%'||?||'%'");
            stmt.setString(1,query);
            stmt.setString(2,query);
            stmt.setString(3,query);
            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                int id = rs.getInt(UserDAOImpl.UserMapping.ID);
                String username = rs.getString(UserDAOImpl.UserMapping.USERNAME);
                String email = rs.getString(UserDAOImpl.UserMapping.EMAIL);
                String pass = "EMPTY RESULT";
                String fName = rs.getString(UserDAOImpl.UserMapping.FNAME);
                String lName = rs.getString(UserDAOImpl.UserMapping.LNAME);
                int auth = rs.getInt(UserDAOImpl.UserMapping.AUTH);
                list.add(new User(id,username,email,pass,fName,lName,auth));
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        logger.info("retrieved {} matches for {} in database",list.size(),query);
        return list;
    }

    @Override
    public User addUser(User user) {

        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("insert into users (username,email,pass,firstname,lastname,auth) values(?,?,?,?,?,?)");

            stmt.setString(1,user.getUsername());
            stmt.setString(2,user.getEmail());
            stmt.setString(3,user.getPassword());
            stmt.setString(4,user.getFirstName());
            stmt.setString(5,user.getLastName());
            stmt.setInt(6,user.getAuth());

            if(stmt.executeUpdate() != 0){
                logger.info("successfully added new User");
                return user;
            }else{
                logger.error("there was an issue adding the item to the database");
                return null;
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        return null;

    }

    @Override
    public User updateUser(User user) {
        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("UPDATE users SET email=?, pass=?, firstname=?, lastname=?, auth=? WHERE id=?");

            stmt.setString(1,user.getEmail());
            stmt.setString(2,user.getPassword());
            stmt.setString(3,user.getFirstName());
            stmt.setString(4,user.getLastName());
            stmt.setInt(5,user.getAuth());
            stmt.setInt(6,user.getId());

            if(stmt.executeUpdate() != 0){
                logger.info("successfully updated User");
                return user;
            }else{
                logger.error("there was an issue updating user in the database");
                return null;
            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
        return null;
    }

    @Override
    public void deleteUser(int id) {
        try {
            connection = ConnectionUtil.getConnection();
            stmt = connection.prepareStatement("DELETE FROM users WHERE id=?");
            stmt.setInt(1,id);
            if(stmt.executeUpdate() != 0){
                logger.info("successfully removed User {}",id);

            }else{
                logger.error("there was an issue removing the user from the database");

            }
        }catch(SQLException e){
            logExceptions(e);
        }finally{
            closeResources();
        }
    }
    private void closeResources() {
        try{
            if(stmt !=null){
                stmt.close();
            }
        } catch (SQLException e) {
            logExceptions(e);
        }
        try{
            if(connection != null){
                connection.close();
            }
        }catch(SQLException e){
            logExceptions(e);
        }
    }
    private void logExceptions(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }
    private static class UserMapping {
        private static final String ID = "id";
        private static final String USERNAME = "username";
        private static final String EMAIL = "email";
        private static final String PASSWORD = "password";
        private static final String FNAME = "firstname";
        private static final String LNAME = "lastname";
        private static final String AUTH = "auth";
    }

}
