package dev.pickett.data;

import dev.pickett.models.User;

import java.util.List;

public interface UserDAO {

    List<User> getAllUsers();
    User getUserById(int id);
    List<User> searchUsers(String query);
    User addUser(User user);
    User updateUser(User user);
    void deleteUser(int id);
}
