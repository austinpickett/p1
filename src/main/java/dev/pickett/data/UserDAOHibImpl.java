package dev.pickett.data;

import dev.pickett.models.User;
import dev.pickett.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


public class UserDAOHibImpl implements UserDAO{
    private final Logger logger = LoggerFactory.getLogger(UserDAOHibImpl.class);


    @Override
    public List<User> getAllUsers() {
        try(Session s = HibernateUtil.getSession()){
            List<User> users = s.createQuery("from users",User.class).list();
            return users;

        }
    }

    @Override
    public User getUserById(int id) {
        try(Session s = HibernateUtil.getSession()){
            User user = s.get(User.class,id);
            return user;
        }
    }

    @Override
    public List<User> searchUsers(String query) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);

            Root<User> root = cq.from(User.class);

            cq.select(root).where(cb.like(root.get("users"),query));
            Query<User> q = s.createQuery(cq);
            return  q.list();
        }
    }

    @Override
    public User addUser(User user) {
        try(Session s = HibernateUtil.getSession() ){
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(user);
            user.setId(id);
            logger.info("Added new User with id:{}",id);
            tx.commit();
            return user;
        }
    }

    @Override
    public User updateUser(User user) {
        try(Session s = HibernateUtil.getSession() ){
            Transaction tx = s.beginTransaction();
            s.update(user);
            logger.info("Updated User with id:{}",user.getId());
            tx.commit();
            return user;
        }
    }

    @Override
    public void deleteUser(int id) {
        try(Session s = HibernateUtil.getSession() ){
            Transaction tx = s.beginTransaction();
            s.delete(new User(id));
            logger.info("Removed User with id:{}",id);
            tx.commit();
        }
    }
}
